Ansible Role keepalived
=========

Install and configure Keepalived

Requirements
------------

- No Requirements

Role Variables
--------------

The default values for the variables are set in `defaults/main.yml`:
```yml
# defaults file for keepalived
start_priority: 100 # default node priority. Will be calculated automatically for MASTER and BACKUP nodes when deploying to inventory group

# Default values for check command if it is enabled in vrrp instance
check_status_command_interval: 2 # check every n seconds
check_status_command_weight: 3 # add n points of prio if OK. This will initiate failover by comparing the priorities.
```

Dependencies
------------
- No Dependencies

Example Inventory
------------

```yml
all:
  hosts:
  children:
    haproxy:
      hosts:
        lbl-haproxy-01:
          ansible_host: 10.199.34.117
        lbl-haproxy-02:
          ansible_host: 10.199.34.118
    keepalived: # Required for role execution when using the apply on inventory group approach to calculate dynamic_property and limit execution
      children:
        haproxy:
```
Example Playbook
----------------
This role can be applied on hosts or an entire inventory group with 2 or more servers.

**Apply on Inventory Group:**

```yml
# Gather facts for all hosts in the inventory group (required for limit task below)
- name: Gather facts when limit_host is defined
  hosts: haproxy
  gather_facts: no
  tasks:
    - name: Gather facts on all hosts
      setup:
      when: limit_host is defined and limit_host | length > 0

# Limit execution to specific inventory host, defaults to all haproxy in this example 
- hosts: "{{ limit_host | default('haproxy') }}"
  become: yes
  gather_facts: yes
  roles:
    - role: keepalived
      keepalived_vrrp_instances:
        - name: VI_1 # Name of VRRP Instance
          interface: eth0 # Optional: Interface where floating virtual IP will be configured. Default is  Ansible fact 'ansible_default_ipv4.interface' fac
          #secondary_ips: # Optional: List of all keepalived nodes. The default will be the default ipv4 address of each host.
          #  - "10.199.34.117" 
          virtual_router_id: 51 # Unique router id
          authentication:
            auth_type: PASS
            auth_pass: "PASS123456!" # Password for communication between keepalived nodes. Please do not use unencrypted passwords. Instead use ansible-vault
          virtual_ipaddresses:
            - name: "192.168.122.27" # Virtual IP that will be configured on active node
              cidr: 24 # netmask
          check_status_command: /etc/keepalived/scripts/haproxy_check.sh # Check command. Can be any command or script. Exit code 0 = successful, >=1 = failed
      keepalived_check_scripts:
        - name: haproxy_check.sh
          content: |
            #!/bin/bash
            /usr/bin/pgrep haproxy
            if [ "$?" == "0" ]; then
                exit 0
            else
                exit 1
            fi
```

Optional: When using the entire inventory group (e.g. `haproxy`), you may utilize the `limit_host` variable to restrict the execution to a specific host while still gathering facts from the `keepalived` inventory hosts.
```bash
ansible-playbook -i <ANSIBLE_INVENTORY> <ANSIBLE_PLAYBOOK>  --extra-vars="limit_host=lbl-haproxy-01"
```

**Apply on single hosts with more granular variables:**
```yml
---
# First keepalived node
- hosts: lbl-haproxy-01
  become: yes
  gather_facts: yes
  roles:
    - role: keepalived
      keepalived_vrrp_instances:
        - name: VI_1 # Name of VRRP Instance
          state: MASTER # First node will be MASTER
          interface: eth0 # Optional: Interface where floating virtual IP will be configured. Default is  Ansible fact 'ansible_default_ipv4.interface' fac
          unicast_src_ip: "10.199.34.117" # IP of first node
          secondary_ips: 
            - "10.199.34.118" # List backup node IPs
          virtual_router_id: 51 # Unique router id
          # Priority: Keep in mind that `check_status_command_weight` will add points to the priorioty value. 
          # Example: 100 (priority) + 3 (check_status_command_weight) = 103 when node is up and check command is successfully. When check command fails priority will be 100
          priority: 100 
          authentication:
            auth_type: PASS 
            auth_pass: "PASS123456!" # Password for communication between keepalived nodes. Please do not use unencrypted passwords. Insead use ansible-vault {{ keepalived_secret }} variable
          virtual_ipaddresses:
            - name: "10.199.34.119" # Virtual IP that will be configured on active node
              cidr: 24 # netmask
          check_status_command: /usr/bin/pgrep haproxy # Check command. Can be any command or script. Exit code 0 = successful, >=1 = failed

# Second keepalived node
- hosts: lbl-haproxy-02
  become: yes
  gather_facts: yes
  roles:
    - role: keepalived
      keepalived_vrrp_instances:
        - name: VI_1
          state: BACKUP # Second node will be BACKUP
          interface: eth0 # Optional: Interface where floating virtual IP will be configured. Default is  Ansible fact 'ansible_default_ipv4.interface' fac
          unicast_src_ip: "10.199.34.118" # IP of second node
          secondary_ips:
            - "10.199.34.117" # List backup node IPs
          virtual_router_id: 51 # Unique route id 
          # Priority: Keep in mind that `check_status_command_weight` will add points to the priorioty value. 
          # Example: 98 (priority) + 3 (check_status_command_weight) = 101 when node is up and check command is successfully. When check command fails priority will be 98
          priority: 98 
          authentication:
            auth_type: PASS
            auth_pass: "PASS123456!" # Password for communication between keepalived nodes. Please do not use unencrypted passwords. Insead use ansible-vault {{ keepalived_secret }} variable
          virtual_ipaddresses:
            - name: "10.199.34.119" 
              cidr: 24
          check_status_command: /usr/bin/pgrep haproxy

```

License
-------

GPLv3

Author Information
------------------

Oleg Franko
